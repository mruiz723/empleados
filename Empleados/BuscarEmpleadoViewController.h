//
//  BuscarEmpleadoViewController.h
//  Empleados
//
//  Created by centro docente de computos on 6/05/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Empleados.h"

@interface BuscarEmpleadoViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *cedulaTxt;

- (IBAction)searchButton:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *statusTxt;
@property (strong, nonatomic) IBOutlet UILabel *labelRegistro;
@property (strong, nonatomic) IBOutlet UILabel *labelCedula;
@property (strong, nonatomic) IBOutlet UILabel *labelNombre;
@property (strong, nonatomic) IBOutlet UILabel *labelDireccion;
@property (strong, nonatomic) IBOutlet UILabel *labelEdad;


@end
