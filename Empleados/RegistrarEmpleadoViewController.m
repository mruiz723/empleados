//
//  RegistrarEmpleadoViewController.m
//  Empleados
//
//  Created by centro docente de computos on 6/05/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "RegistrarEmpleadoViewController.h"
#import "Empleados.h"

@interface RegistrarEmpleadoViewController ()

@end

@implementation RegistrarEmpleadoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    nuevoEmpleado = [[Empleados alloc]init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
// me devuelve por cada view dejar de responder
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    for (UIView * view in self.view.subviews) {
        [view resignFirstResponder];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)saveEmp:(id)sender {
    nuevoEmpleado.empCedula = _empCedulaNew.text;
    nuevoEmpleado.empName = _empNameNew.text;
    nuevoEmpleado.empAge = _empAgeNew.text;
    nuevoEmpleado.empAdress = _empAdressNew.text;
    
    [nuevoEmpleado createEmployedInDataBase];
    _statusText.text =nuevoEmpleado.status;
}
@end
